<!DOCTYPE html>
<html>

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body class="mobile_fixed lk_page">

        <div class="page_wp page_bg1">

            <!-- Header -->
            <?php include('inc/header_lk.inc.php') ?>
            <!-- -->

            <!-- Top menu -->
            <?php include('inc/topmenu.inc.php') ?>
            <!-- -->


            <div class="consent_wp lk_content my_lk">
                <div class="wrapper">
                    <div class="lk_wp2 flex  justify-content-between">

                        <div class="lk_left">

                            <div class="loan_title lt_mob_f1">
                                <div class="text_uppercase">Открытый заем</div>
                                <div class="loan_title_second">У Вас нет активного займа</div>
                            </div>

                            <div class="account">
                                <div class="account__timer_text">Так как предыдущий заем был погашен досрочно, Вы сможете отправить заявку на новый заем только через
                                    <strong class="text_nowrap">7 дней.</strong>
                                </div>
                                <div class="account__timer">
                                    <div class="account__timer_box">
                                        <div class="account__timer_title">Вы сможете отправить <span class="block_xs">заявку на заем</span></div>
                                        <div class="account__timer_value">10.09.2019</div>
                                        <div class="account__timer_progress"><span style="width: 65%;"></span></div>
                                    </div>
                                    <div class="account__timer_date">Осталось 2 дня </div>
                                </div>

                            </div>

                        </div>

                        <div class="lk_right visible_lg">
                            <div class="mob_bonus_f mob_bonus_one bb_p_dn">
                                <div class="bonus_block">
                                    <p class="bonus_title">Мои Бонусы</p>
                                    <div class="bonus_info flex align-items-center justify-content-center">
                                        <img src="images/ico41.png" alt=""/><p>500</p>
                                    </div>
                                    <div class="lk_links flex justify-content-between">
                                        <a>Потратить</a>
                                        <a>Заработать</a>
                                    </div>
                                </div>
                            </div>
                            <div class="bonus_block bonus_forma">
                                <p class="bonus_title">Написать сообщение</p>
                                <form class="bonus_form">
                                    <div class="bf_mob_wp">
                                        <div class="bf_mob">
                                            <div class="input_wp">
                                                <select data-placeholder="Пол">
                                                    <option></option>
                                                    <option>Тема сообщения</option>
                                                    <option>Тема сообщения</option>
                                                </select>
                                                <p>Тема сообщения</p>
                                            </div>
                                        </div>
                                        <div class="input_wp bonus_pole_wp">
                                            <textarea class="bonus_pole"></textarea>
                                            <p>Текст сообщения</p>
                                        </div>
                                    </div>
                                    <div class="bf_mob1">
                                        <div class="add_file af_mob_m1 flex align-items-center justify-content-center">
                                            <a><img src="images/ico43.png" alt=""/><span>Прикрепить файл</span></a>
                                        </div>
                                        <div class="lkf_bot lkf_mob_gray">
                                            <a data-fancybox href="#modal5" class="bf_btn">Отправить</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer LC -->
            <?php include('inc/footer_lk.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->


        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
