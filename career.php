<!DOCTYPE html>
<html>

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body class="mobile_fixed">

        <div class="page_wp page_bg1">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Top menu -->
            <?php include('inc/topmenu.inc.php') ?>
            <!-- -->

            <div class="breadcrumbs">
                <div class="wrapper">
                    <div class="breadcrumb">
                        <span class="inline odd first"><a href="/">Главная</a></span>
                        <span class="delimiter">&gt;</span>
                        <span class="inline even last">Клиентам</span>
                    </div>
                </div>
            </div>

            <div class="heading heading_border">
                <div class="wrapper">
                    <div class="heading__inner">
                        <div class="heading__wrap">
                            <div class="heading__title">
                                <div class="heading__title_second">Клиентам</div>
                                <h1>Карьера в Мультизайм</h1>
                            </div>
                            <div class="heading__box">
                                <div class="heading__content">
                                    <h3 class="heading__subtitle">Давайте работать <span class="block_xs red_xs">вместе</span></h3>
                                    <div class="heading__image_mobile">
                                        <img src="img/career__image.jpg" class="img_fluid" alt="">
                                    </div>
                                    <div class="heading__text">Мы открыты для новых идей и рады интересным людям в нашей команде. Если Вы чувствуете драйв, Вас увлекают большие сложные задачи - добро пожаловать в Мультизайм!</div>

                                </div>
                            </div>
                        </div>
                        <div class="heading__image heading__image_tablet">
                            <img src="img/career__bg_tablet.png" class="img_fluid" alt="">
                        </div>
                        <div class="heading__image heading__image_desktop">
                            <img src="img/career__bg_desktop.png" class="img_fluid" alt="">
                        </div>
                    </div>
                </div>
            </div>

            <section class="main">
                <div class="wrapper">
                    <div class="main__mobile">
                        <div class="main__heading">
                            <h2><span class="main__heading_red">Мы предлагаем</span> сотрудникам</h2>
                        </div>
                        <div class="career__row">
                            <div class="career">
                                <div class="career__image">
                                    <div class="career__image_wrap">
                                        <img src="img/career__image_01.jpg" class="img_fluid" alt="">
                                    </div>
                                </div>
                                <div class="career__text">
                                    <div class="career__text_wrap">
                                        Быстрый
                                        рост
                                        зарплаты
                                    </div>
                                </div>
                            </div>
                            <div class="career">
                                <div class="career__image">
                                    <div class="career__image_wrap">
                                        <img src="img/career__image_02.jpg" class="img_fluid" alt="">
                                    </div>
                                </div>
                                <div class="career__text">
                                    <div class="career__text_wrap">
                                        Бонусы
                                    </div>
                                </div>
                            </div>
                            <div class="career">
                                <div class="career__image">
                                    <div class="career__image_wrap">
                                        <img src="img/career__image_03.jpg" class="img_fluid" alt="">
                                    </div>
                                </div>
                                <div class="career__text">
                                    <div class="career__text_wrap">
                                        Дружный
                                        коллектив
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="career__content">Компания стремительно развивается, каждый день появляются новые интересные вакансии, свяжитесь с нами, что бы узнать об актуальных позициях. Направляйте Ваши резюме на почту: <strong>hr@mz24.ru</strong></div>
                    </div>
                </div>
            </section>

            <div class="how_to">
                <div class="wrapper">
                    <div class="how_to__box">
                        <div class="how_to__content">
                            <div class="how_to__heading">Всего <span class="color_red">15 минут</span></div>
                            <div class="how_to__text">и деньги у Вас на карте</div>
                            <div class="how_to__image">
                                <img src="images/img22.png" alt="" class="img_fluid">
                            </div>
                        </div>
                        <div class="how_to__button">
                            <a href="#" class="btn btn_red btn_shadow">Получить деньги</a>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer Base -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->


        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
