<!DOCTYPE html>
<html>

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body class="mobile_fixed lk_page">

        <div class="page_wp page_gray">

            <!-- Header -->
            <?php include('inc/header_lk.inc.php') ?>
            <!-- -->

            <!-- Top menu -->
            <?php include('inc/topmenu.inc.php') ?>
            <!-- -->

            <div class="box">
                <div class="box__heading">
                    <span class="block_sm">Модальные окна</span>
                </div>
                <div class="box__content box__content_white">
                    <a href="#modal6" class="code_repeat" data-fancybox>Modal One</a>
                    <br/>
                    <a href="#extension_success" class="code_repeat" data-fancybox>Modal two</a>
                </div>
                <div class="box__footer">

                </div>
            </div>


            <!-- Footer LC -->
            <?php include('inc/footer_lk.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->


        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
