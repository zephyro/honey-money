<!DOCTYPE html>
<html>

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body class="mobile_fixed">

        <div class="page_wp page_bg1">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Top menu -->
            <?php include('inc/topmenu.inc.php') ?>
            <!-- -->

            <div class="breadcrumbs">
                <div class="wrapper">
                    <div class="breadcrumb">
                        <span class="inline odd first"><a href="/">Главная</a></span>
                        <span class="delimiter">&gt;</span>
                        <span class="inline even last">Бонусная программа</span>
                    </div>
                </div>
            </div>

            <div class="heading heading_bonus heading_border">
                <div class="wrapper">
                    <div class="heading__inner">
                        <div class="heading__wrap">
                            <div class="heading__title">
                                <div class="heading__title_second">Клиентам</div>
                                <h1>Бонусная программа</h1>
                            </div>
                            <div class="heading__box">
                                <div class="heading__content">
                                    <h3 class="heading__subtitle">Для чего нужны бонусы</h3>
                                    <div class="heading__image_mobile">
                                        <img src="img/bonus__image.jpg" class="img_fluid" alt="">
                                    </div>
                                    <div class="heading__text">В сервисе Мультизайм можно накапливать и тратить бонусные баллы. Это приятно и позволяет вам экономить на процентной ставке и получать дополнительные преимущества.</div>

                                </div>
                            </div>
                        </div>
                        <div class="heading__image heading__image_tablet heading__image_comments">
                            <img src="img/bonus__bg_tablet.png" class="img_fluid" alt="">
                        </div>
                        <div class="heading__image heading__image_desktop heading__image_comments">
                            <img src="img/bonus__bg_desktop.png" class="img_fluid" alt="">
                        </div>
                    </div>
                </div>
            </div>

            <div class="bonus bonus_gray">
                <div class="wrapper">
                    <div class="bonus__mobile">
                        <h2 class="bonus__heading">Как заработать <span class="color_red_sm">бонусы</span></h2>
                        <div class="bonus__row">
                            <div class="bonus__item">
                                <div class="bonus__top">
                                    <i><img src="img/bonus__icon_01.png" class="img_fluid" alt=""></i>
                                    <span>Пригласите друга</span>
                                </div>
                                <div class="bonus__text">
                                    <p>Пригласите друга. Если он оформит
                                        заём, Вы получите бонусные баллы
                                        на свой счет.</p>
                                   <div class="text_center">
                                       <a href="#">Пригласить</a>
                                   </div>
                                </div>
                            </div>
                            <div class="bonus__item">
                                <div class="bonus__top">
                                    <i><img src="img/bonus__icon_02.png" class="img_fluid" alt=""></i>
                                    <span>Напишите отзыв</span>
                                </div>
                                <div class="bonus__text">
                                    <p>
                                        За каждый написанный отзыв вам
                                        начисляются бонусные баллы.
                                    </p>
                                    <div class="text_center">
                                        <a href="#">Написать</a>
                                    </div>
                                </div>
                            </div>
                            <div class="bonus__item">
                                <div class="bonus__top">
                                    <i><img src="img/bonus__icon_03.png" class="img_fluid" alt=""></i>
                                    <span>Верните займ <br/>без просрочки</span>
                                </div>
                                <div class="bonus__text">
                                    <p>
                                        За каждый вовремя погашенный
                                        займ вам начисляются бонусные
                                        баллы.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="bonus__content">
                            Чем больше Вы приглашаете друзей или чем чаще погашаете заем вовремя, тем больше Вы накапливаете бонусных баллов. За каждый написанный отзыв на нашем сайте, Вы так же получаете бонусы. Накопленные бонусные баллы отображаются на странице «Мои бонусы» в личном кабинете.
                        </div>
                    </div>
                </div>
            </div>

            <div class="bonus">
                <div class="wrapper">
                    <div class="bonus__mobile">
                        <h2 class="bonus__heading">Как потратить <span class="color_red_sm">бонусы</span></h2>
                        <div class="bonus__row">
                            <div class="bonus__item bonus__item_two">
                                <div class="bonus__top">
                                    <i><img src="img/bonus__icon_04.png" class="img_fluid" alt=""></i>
                                    <span>Погашайте <br/>проценты по займу</span>
                                </div>
                                <div class="bonus__text">
                                    <p>
                                        Экономьте деньги, оплачивайте проценты за пользование займом бонусами
                                    </p>
                                    <div class="text_center">
                                        <a href="#">Погасить</a>
                                    </div>
                                </div>
                            </div>
                            <div class="bonus__item bonus__item_two">
                                <div class="bonus__top">
                                    <i><img src="img/bonus__icon_05.png" class="img_fluid" alt=""></i>
                                    <span>Продлевайте<br/>срок займа</span>
                                </div>
                                <div class="bonus__text">
                                    <p>
                                        Не успеваете погасить заем в срок? Перенесите дату возврата и оплатите это бонусами
                                    </p>
                                    <div class="text_center">
                                        <a href="#">Продлить</a>
                                    </div>
                                </div>
                            </div>
                            <div class="bonus__item bonus__item_two">
                                <div class="bonus__top">
                                    <i><img src="img/bonus__icon_06.png" class="img_fluid" alt=""></i>
                                    <span>Увеличивайте<br/>кредитный лимит</span>
                                </div>
                                <div class="bonus__text">
                                    <p>
                                        Используйте бонусы для оплаты увеличения лимита по займу. Берите больше денег!
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="bonus__content">
                            Потратить бонусные баллы Вы можете на погашение процентов по займу, на продление срока займа или же на увеличение лимита займа. Накопленные бонусы не сгорают, потратить их возможно в любое время.
                        </div>
                    </div>
                </div>
            </div>

            <div class="how_to">
                <div class="wrapper">
                    <div class="how_to__box">
                        <div class="how_to__content">
                            <div class="how_to__heading">Всего <span class="color_red">15 минут</span></div>
                            <div class="how_to__text">и деньги у Вас на карте</div>
                            <div class="how_to__image">
                                <img src="images/img22.png" alt="" class="img_fluid">
                            </div>
                        </div>
                        <div class="how_to__button">
                            <a href="#" class="btn btn_red btn_shadow">Получить деньги</a>
                        </div>
                    </div>
                </div>
            </div>



            <!-- Footer Base -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->


        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
