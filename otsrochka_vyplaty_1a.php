<!DOCTYPE html>
<html>

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body class="mobile_fixed lk_page">

        <div class="page_wp page_bg1">

            <!-- Header -->
            <?php include('inc/header_lk.inc.php') ?>
            <!-- -->

            <!-- Top menu -->
            <?php include('inc/topmenu.inc.php') ?>
            <!-- -->


            <div class="consent_wp lk_content my_lk">
                <div class="wrapper">
                    <div class="lk_wp2 flex justify-content-between">

                        <div class="lk_left lk_left__gray">

                            <form class="form">

                                <div class="loan_title lt_mob_f1"><div class="text_uppercase">Отсрочка выплаты</div></div>

                                <div class="account_gray">
                                    <div class="account">

                                        <div class="account__heading">
                                            <div class="account__heading_title text_center">Подпишите соглашение о продлении</div>
                                            <div class="account__heading_text mb_30">Для оформления продления займа Вам необходимо ознакомиться и подписать дополнительное соглашение к договору займа.</div>
                                            <label class="form_checkbox mw_350">
                                                <input type="checkbox" name="check" value="check" checked>
                                                <span>Согласен с условиями <a href="#">доп. соглашения о продлении займа</a></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="account__submit">
                                    <button type="submit" class="btn btn_red btn_shadow">Подписать</button>
                                </div>


                            </form>

                        </div>

                        <div class="lk_right hide_xs visible_lg">

                            <div class="mob_bonus_f mob_bonus_one tablet_bonus_none">
                                <div class="bonus_block">
                                    <p class="bonus_title">Мои Бонусы</p>
                                    <div class="bonus_info flex align-items-center justify-content-center">
                                        <img src="images/ico41.png" alt=""/><p>500</p>
                                    </div>
                                    <div class="lk_links flex justify-content-between">
                                        <a>Потратить</a>
                                        <a>Заработать</a>
                                    </div>
                                </div>
                            </div>

                            <div class="bonus_block bonus_forma">
                                <p class="bonus_title">Написать сообщение</p>
                                <form class="bonus_form">
                                    <div class="bf_mob_wp">
                                        <div class="bf_mob">
                                            <div class="input_wp">
                                                <input class="bonus_input" type="text"/>
                                                <p>E-mail</p>
                                            </div>
                                        </div>
                                        <div class="input_wp bonus_pole_wp">
                                            <textarea class="bonus_pole"></textarea>
                                            <p>Текст сообщения</p>
                                        </div>
                                    </div>
                                    <div class="bf_mob1">
                                        <div class="add_file af_mob_m1 flex align-items-center justify-content-center">
                                            <a><img src="images/ico43.png" alt=""/><span>Прикрепить файл</span></a>
                                        </div>
                                        <div class="lkf_bot lkf_mob_gray">
                                            <a data-fancybox href="#modal5" class="bf_btn">Отправить</a>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer LC -->
            <?php include('inc/footer_lk.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->


        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
