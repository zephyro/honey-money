<!DOCTYPE html>
<html>

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body class="mobile_fixed">

        <div class="page_wp page_bg1">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Top menu -->
            <?php include('inc/topmenu.inc.php') ?>
            <!-- -->

            <div class="breadcrumbs">
                <div class="wrapper">
                    <div class="breadcrumb">
                        <span class="inline odd first"><a href="/">Клиентам</a></span>
                        <span class="delimiter">&gt;</span>
                        <span class="inline even last">Документы</span>
                    </div>
                </div>
            </div>

            <div class="heading heading_border">
                <div class="wrapper">
                    <div class="heading__inner">
                        <div class="heading__wrap">
                            <div class="heading__title">
                                <div class="heading__title_second">Клиентам</div>
                                <h1>Как перенести дату <span class="block_xs">возврата</span></h1>
                            </div>
                            <div class="heading__box">
                                <div class="heading__content">
                                    <h3 class="heading__subtitle"><span class="red_xs">Очень просто и удобно</span></h3>
                                    <div class="heading__image_mobile">
                                        <img src="img/return_date__image.jpg" class="img_fluid" alt="">
                                    </div>
                                    <div class="heading__text">Если Вы не успеваете погасить заем в срок, Вы можете воспользоваться услугой «Отсрочка выплаты». Это позволит перенести оплату на удобную Вам дату.</div>

                                </div>
                            </div>
                        </div>
                        <div class="heading__image heading__image_tablet">
                            <img src="img/return_date__bg_tablet.png" class="img_fluid" alt="">
                        </div>
                        <div class="heading__image heading__image_desktop">
                            <img src="img/return_date__bg_desktop.png" class="img_fluid" alt="">
                        </div>
                    </div>
                </div>
            </div>

            <div class="deferral">
                <div class="wrapper">
                    <div class="deferral__container">
                        <h3 class="deferral__title"><span>Преимущества</span> отсрочки выплаты</h3>
                        <div class="deferral__row">
                            <div class="deferral__item deferral__item_one">
                                <div class="deferral__item_number"><span>1</span></div>
                                <div class="deferral__item_text">
                                    Не начисляются штрафы<br/>
                                    <small>(На время действия отсрочки)</small>
                                </div>
                            </div>
                            <div class="deferral__item deferral__item_two">
                                <div class="deferral__item_number"><span>2</span></div>
                                <div class="deferral__item_text">Не портите свою <br/>кредитную историю</div>
                            </div>
                            <div class="deferral__item deferral__item_three">
                                <div class="deferral__item_number"><span>3</span></div>
                                <div class="deferral__item_text">Сами выбираете, <br/>когда вернуть деньги</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="manual">
                <div class="wrapper">
                    <div class="mob_p1">

                        <h2 class="manual__heading" style="max-width: 440px;"><span class="block_xs">Как оформить </span>отсрочку оплаты</h2>

                        <div class="instruction_wp">

                            <div class="instruction_block flex justify-content-between">
                                <div class="instruction_img">

                                </div>
                                <div class="instruction_info">
                                    <div id="block-block-86" class="block block-block">
                                        <div class="content">
                                            <p class="ii_title"><span>Выберите </span> срок</p>
                                            <p class="ii_text">В личном кабинете на странице "Открытый заем" нажмите кнопку "Продлить". Откроется траница "Отсрочка выплаты". На ней выберите срок на который хотите перенести дату возврата займа и нажмите кнопку "Оформить продление".</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="instruction_block ib_v1 flex justify-content-between">
                                <div class="instruction_img">

                                </div>
                                <div class="instruction_info">
                                    <div id="block-block-87" class="block block-block">
                                        <div class="content">
                                            <p class="ii_title"><span>Подпишите</span> соглашение о продлении</p>
                                            <p class="ii_text">Ознакомьтесь с условиями соглашения и нажмите кнопку «Подписать». Вам придёт смс с кодом, который нужно указать на следующей странице «Подтверждение согласия с условиями доп. соглашения».</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="instruction_block flex justify-content-between">
                                <div class="instruction_img">

                                </div>
                                <div class="instruction_info">
                                    <div id="block-block-86" class="block block-block">
                                        <div class="content">
                                            <p class="ii_title"><span>Оплатите</span> продление</p>
                                            <p class="ii_text">Оплатите продление займа. Нажмите кнопку «Оплатить» и с Вашей карты спишется сумма за продление. После оплаты появится сообщение «Отсрочка выплаты успешно оформлена».</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                    </div>
                    </div>
                </div>
            </div>

            <div class="how_to">
                <div class="wrapper">
                    <div class="how_to__box">
                        <div class="how_to__content">
                            <div class="how_to__heading">Всего <span class="color_red">15 минут</span></div>
                            <div class="how_to__text">и деньги у Вас на карте</div>
                            <div class="how_to__image">
                                <img src="images/img22.png" alt="" class="img_fluid">
                            </div>
                        </div>
                        <div class="how_to__button">
                            <a href="#" class="btn btn_red btn_shadow">Получить деньги</a>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer Base -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->


        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
