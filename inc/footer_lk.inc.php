<div class="footer_down"></div>
<footer>

    <div class="footer_lk_desctop">
        <div class="lk_f_info">
            <div class="wrapper flex align-items-center justify-content-center">
                <a class="header_phone">8 (800) 222 74 34</a><span>Служба поддержки клиентов</span>
            </div>
        </div>
        <div class="footer_bot lk_f_bot">
            <div class="wrapper">
                <div class="mob_bg flex justify-content-center">
                    <p class="copyright">©  «MZ24.RU», 2019</p>
                </div>
            </div>
        </div>
    </div>

    <div class="footer_lk_mobile">
        <div class="footer_top">
            <div class="wrapper">
                <div class="mob_bg flex">
                    <div class="f_column fc_w1">
                        <div class="f_logo">
                            <a>
                                <img src="images/logo2.png" alt=""/>
                            </a>
                        </div>
                        <p class="fc_text">
                            Номер записи в реестре<br/> МФО ЦБ РФ<br/> №1703045008571
                        </p>
                        <div class="fc_social">
                            <p>Всегда на связи с Вами</p>
                            <div class="flex">
                                <a class="vk_soc"></a>
                                <a class="fb_soc"></a>
                                <a class="ig_soc"></a>
                            </div>
                        </div>
                        <div class="mob_f_phone">
                            <a class="header_phone">8 (800) 222 74 34</a>
                            <p class="cb_title">Служба поддержки клиентов</p>
                            <a class="mobile_q_btn">Задать вопрос</a>
                        </div>
                    </div>
                    <div class="f_column fc_w2">
                        <p class="fm_title">Заемщикам</p>
                        <ul class="f_list">
                            <li>
                                <a>Личный кабинет</a>
                            </li>
                            <li>
                                <a>Как это работает</a>
                            </li>
                            <li>
                                <a>Наши ставки</a>
                            </li>
                            <li>
                                <a>Как получить </a>
                            </li>
                            <li>
                                <a>Как погасить</a>
                            </li>
                            <li>
                                <a>Вопросы и ответы</a>
                            </li>
                            <li>
                                <a>О компании</a>
                            </li>
                            <li>
                                <a>Новости и акции</a>
                            </li>
                            <li>
                                <a>Бонусная программа</a>
                            </li>
                            <li>
                                <a>Отзывы</a>
                            </li>
                        </ul>
                    </div>
                    <div class="f_column fc_w3">
                        <p class="fm_title">Общая информация</p>
                        <ul class="f_list">
                            <li>
                                <a>Документы</a>
                            </li>
                            <li>
                                <a>Статьи</a>
                            </li>
                            <li>
                                <a>Контакты</a>
                            </li>
                            <li>
                                <a>Партнерская программа</a>
                            </li>
                            <li>
                                <a>Карьера</a>
                            </li>
                            <li>
                                <a>Карта сайта</a>
                            </li>
                        </ul>
                    </div>
                    <div class="f_column fc_w4">
                        <a class="header_phone">8 (800) 222 74 34</a>
                        <div class="contacts_block">
                            <p class="cb_title">Служба поддержки клиентов</p>
                            <p class="cb_text"><span></span> <a>ask@hunny-money.ru</a></p>
                            <p class="cb_text"><span></span> <a>www.hunny-money.ru</a></p>
                            <p class="cb_text"><span></span> г. Москва, ул Антонова-<br/>
                                Овсеенко, д.15.  стр. 3, ком. 9</p>
                        </div>
                        <div class="company_info">
                            <p class="ci_title">Услуги предоставляет:</p>
                            <p class="ci_text">
                                <span>ООО «ММК Автокапитал»</span>
                                <span>ОГРН: 5177746069584</span>
                                <span>ИНН: 9710039070</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer_bot">
            <div class="wrapper">
                <div class="mob_bg">
                    <div class="mob_company_info">
                        <div class="company_info">
                            <p class="ci_title">Услуги предоставляет:</p>
                            <p class="ci_text">
                                <span>ООО «ММК Автокапитал»</span>
                                <span>ОГРН: 5177746069584</span>
                                <span>ИНН: 9710039070</span>
                            </p>
                        </div>
                    </div>
                    <div class="f_logo_mob">
                        <a>
                            <img class="dl" src="images/logo2.png" alt=""/>
                            <img class="ml" src="images/logo3.png" alt=""/>
                        </a>
                    </div>
                    <p class="copyright">©  «MZ24.RU», 2019</p>
                </div>
            </div>
        </div>
    </div>

</footer>
