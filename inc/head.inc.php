<head>
    <title>pagetitle</title>
    <meta charset="utf-8">
    <meta id="viewport" name="viewport" content="width=device-width">

    <script>
        if (screen.width <= 1199 && screen.width >= 768) {
            var mvp = document.getElementById('viewport');
            mvp.setAttribute('content','width=768');
        }
        else if (screen.width <= 767) {
            var mvp = document.getElementById('viewport');
            mvp.setAttribute('content','width=320');
        }
    </script>

    <meta name="description" content="">
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" type="image/png" href="favicon.png" />
    <meta name="keywords" content="">
    <link rel="stylesheet" href="css/swiper.css" />
    <link rel="stylesheet" href="css/jquery.fancybox.min.css" />
    <link rel="stylesheet" href="css/ion.rangeSlider.css" />
    <link rel="stylesheet" href="css/jquery.formstyler.css" />
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/media.css" />

    <!-- update -->
    <link rel="stylesheet" href="js/mCustomScrollbar/jquery.mCustomScrollbar.min.css" />
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <link rel="stylesheet" href="css/main.css" />
</head>


