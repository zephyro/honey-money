<header>
    <div class="wrapper flex align-items-center justify-content-between">
        <div class="header_left flex align-items-center">
            <a href="#" class="mm_link"></a>
            <div class="logo">
                <a href="/">
                    <img src="images/logo_new.svg" alt="">
                </a>
            </div>
            <div class="hl_text">

                <div id="block-block-1" class="block block-block">


                    <div class="content">
                        <p>Первый заем&nbsp;-<br><span>бесплатно</span> (0%)</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="header_right flex align-items-center">

            <div>

                <div id="block-block-2" class="block block-block">


                    <div class="content">
                        <p><a class="header_phone" href="tel:88002227434">8 (800) 222 74 34</a></p>
                    </div>
                </div>


            </div>
            <div class="hr_text">
                <div id="block-block-23" class="block block-block">


                    <div class="content">
                        <p><span>24</span> часа,<br> без выходных</p>
                    </div>
                </div>

            </div>


            <a href="/auth/" class="button lk_btn"><span>Личный кабинет</span></a>
        </div>
    </div>
</header>
