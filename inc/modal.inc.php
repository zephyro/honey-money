<div class="modal modal_w1" id="modal1">
    <div class="thank_wp">
        <div class="thank_img">
            <img src="images/ico65.png" alt=""/>
        </div>
        <p class="thank_title">Спасибо за заявку</p>
        <p class="thank_text1">Подача заявок для Вас временно<br/>
            <span>заблокирована</span></p>
        <p class="thank_text2">Вы можете подать заявку<br/>
            повторно <span>не ранее</span> 23.05.2019</p>
    </div>
</div>

<div class="modal modal_w2 modal_p1" id="modal2">
    <div class="m_order">
        <p class="login_title">
            Получите деньги
        </p>
        <div class="test_wp">
            <p class="test_text">
                Вам предварительно<br/>
                одобрен заем на <span>20 000р</span>!
            </p>
            <div class="test_img">
                <img class="ti_d" src="images/img18.jpg" alt=""/>
                <img class="ti_m" src="images/img18.jpg" alt=""/>
            </div>
            <p class="thank_text2">Выберите срок, подтвердите договор
                оферты и мы моментально переведем
                деньги на Вашу карту</p>
        </div>
        <div class="lkf_bot">
            <a class="consent_btn">Продолжить</a>
            <a class="bf_btn">Взять меньшую сумму</a>
            <div class="add_file flex align-items-center justify-content-center">
                <a><img src="images/ico66.png" alt=""/><span>Отказаться</span></a>
            </div>
        </div>
    </div>
</div>

<div class="modal modal_w2 modal_p1" id="modal3">
    <div class="m_order">
        <p class="login_title">
            Проверка заявки
        </p>
        <div class="test_wp">
            <p class="test_title">
                Ваша заявка <span>успешно</span> отправлена!
            </p>
            <p class="test_subtitle">Сейчас она находится на проверке.<br/>
                Это займет несколько минут.</p>
            <div class="test_img">
                <img class="ti_d" src="images/img19.jpg" alt=""/>
                <img class="ti_m" src="images/img19.jpg" alt=""/>
            </div>
            <p class="test_subtitle ts_mn">По окончании проверки Вам
                на телефон прийдет смс с решением
                по заявке.</p>
        </div>
    </div>
</div>

<div class="modal modal_w2 modal_p1" id="modal4">
    <div class="m_order">
        <div class="loan_title loan_title_bn">
            <p>ПОЗДРАВЛЯЕМ!
                <span>Ваша заявка одобрена</span>
            </p>
        </div>
        <div class="test_wp">
            <div class="test_img">
                <img class="ti_d" src="images/img18.jpg" alt=""/>
                <img class="ti_m" src="images/img18.jpg" alt=""/>
            </div>
            <p class="thank_text2">Выберите срок, подтвердите договор
                оферты и мы моментально переведем
                деньги на Вашу карту</p>
            <div class="lkf_bot">
                <a class="consent_btn">Получить деньги</a>
            </div>
        </div>
    </div>
</div>

<div class="modal modal_w2 modal_p1" id="modal5">
    <div class="m_order">
        <div class="test_wp">
            <div class="test_img">
                <img class="ti_d" src="images/img9.png" alt=""/>
                <img class="ti_m" src="images/img9.png" alt=""/>
            </div>
            <p class="thank_text2 tt_m1"><span>Ожидайте перечисления<br/>
        денежных средств на карту</span></p>
            <div class="lkf_bot lkf_b_bg1">
                <p class="test_subtitle ts_mn">Выберите срок, подтвердите
                    договор оферты и мы моментально
                    переведем деньги на Вашу карту</p>
            </div>
        </div>
    </div>
</div>


<!-- Продление займа успешно оформлено!-->

<div class="modal modal_w1" id="modal6">
    <div class="thank_wp">
        <div class="thank_img">
            <img src="images/ico65.png" alt=""/>
        </div>
        <div class="thank_title">Продление займа<br/><span class="color_black">успешно оформлено!</span></div>
        <div class="thank_sum">
            <div class="thank_sum__item">
                <span>Новая дата возврата:</span>
                <strong>12.09.2019</strong>
            </div>
            <div class="thank_sum__item">
                <span>Сумма к погашению:</span>
                <strong>10 700.00 Р</strong>
            </div>
        </div>
    </div>
</div>

<!-- Досрочное погашение займа -->

<div class="modal modal_w1" id="extension_success">
    <div class="modal_wrap">
        <div class="modal_image">
            <img src="img/modal__ruble.png" alt=""/>
        </div>
        <div class="thank_title">Досрочное погашение <br/><span class="color_black">займа</span></div>
        <div class="modal_box">В случае досрочного погашения займа Вы сможете отправить заявку на новый заем только <strong class="text_nowrap">через 7 дней</strong></div>
        <div class="modal_bottom">
            <a href="#" class="modal_link">Продолжить</a>
        </div>
    </div>
</div>

<!-- Окно с текстом-->

<div class="modal modal_w1" id="modal_test">
    <div class="thank_wp">
       <div class="text_center">Nam ullamcorper urna quis purus iaculis, quis pellentesque dui efficitur. Proin fringilla eleifend dolor, vel gravida diam. Etiam blandit nunc sit amet pretium lobortis.</div>
    </div>
</div>