<header class="lk_header">
    <div class="wrapper flex align-items-center justify-content-between">
        <div class="header_left flex align-items-center">
            <a href="#" class="mm_link"></a>
            <div class="logo">
                <a href="#" style="cursor:pointer;" onclick="window.top.location.href='/'">
                    <img src="images/logo_new.svg" alt="">
                </a>
            </div>
            <p class="hl_text">
                Первый заем -<br>
                <span>бесплатно</span> (0%)
            </p>
        </div>
        <div class="header_right flex align-items-center">
            <a href="tel:+78002227434" class="header_phone">8 (800) 222 74 34</a>
            <div class="lk_h_block flex align-items-center">
                <a href="#" class="notice_link "></a>
                <p class="name_text">Иван Кириллов</p>
                <a href="#" class="my_data "><span>Мои  данные</span></a>
                <a href="#" class="exit_link"><span>Выйти</span></a>
            </div>
        </div>
    </div>
</header>