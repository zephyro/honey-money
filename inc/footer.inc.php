<div class="footer_down"></div>
<footer>
    <div class="footer_top">
        <div class="wrapper">
            <div class="mob_bg flex">
                <div class="f_column fc_w1">
                    <div class="f_logo">
                        <a href="/">
                            <img src="images/logo_new.svg" alt="">
                        </a>
                    </div>
                    <div id="block-block-25" class="block block-block">
                        <div class="content">
                            <p class="fc_text">
                                Номер записи в реестре<br> МФО ЦБ РФ<br> №1703045008571
                            </p>
                            <div class="fc_social">
                                <p>Всегда на связи с Вами</p>
                                <div class="flex">
                                    <a class="vk_soc"></a><br><a class="fb_soc"></a><br><a class="ig_soc"></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="mob_f_phone">
                        <div>
                            <div id="block-block-2" class="block block-block">
                                <div class="content">
                                    <p><a class="header_phone" href="tel:88002227434">8 (800) 222 74 34</a></p>
                                </div>
                            </div>
                        </div>
                        <p class="cb_title">Служба поддержки клиентов</p>
                    </div>
                </div>
                <div class="f_column fc_w2">
                    <p class="fm_title">ЗАЕМЩИКАМ</p>
                    <ul class="f_list">
                        <li><a href="#" class="mm_btn2 mm_btn2_foot">Личный кабинет</a></li>
                        <li><a href="#" class="">Как это работает</a></li>
                        <li><a href="#" class="">Наши ставки</a></li>
                        <li><a href="#" class="">Как получить</a></li>
                        <li><a href="#" class="">Как погасить</a></li>
                        <li><a href="#" class="">Вопросы и ответы</a></li>
                        <li><a href="#" class="">О компании</a></li>
                        <li><a href="#" class="">Новости и акции</a></li>
                        <li><a href="#" class="">Бонусная программа</a></li>
                        <li><a href="#" class="">Отзывы</a></li>
                    </ul>
                </div>
                <div class="f_column fc_w3">
                    <p class="fm_title">ОБЩАЯ ИНФОРМАЦИЯ</p>
                    <ul class="f_list">
                        <li><a href="#" class="">Документы</a></li>
                        <li><a href="#" class="">Статьи</a></li>
                        <li><a href="#" class="">Контакты</a></li>
                        <li><a href="#" class="">Партнерская программа</a></li>
                        <li><a href="#" class="">Карьера</a></li>
                        <li><a href="#" class="">Карта сайта</a></li>
                    </ul>
                </div>
                <div class="f_column fc_w4">
                    <div>
                        <div id="block-block-2" class="block block-block">
                            <div class="content">
                                <p><a class="header_phone" href="tel:88002227434">8 (800) 222 74 34</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="contacts_block">
                        <div id="block-block-26" class="block block-block">
                            <div class="content">
                                <p class="cb_title">Служба поддержки клиентов</p>
                                <p class="cb_text">&nbsp;<a class="cbt_col1" href="mailto:ask@mz24.ru">ask@mz24.ru</a></p>
                                <p class="cb_text">&nbsp;<a href="mz24.ru">www.mz24.ru</a></p>
                                <p class="cb_text">&nbsp;г. Москва, ул Антонова-<br>
                                    Овсеенко, д.15. стр. 3, ком. 9</p>
                            </div>
                        </div>
                    </div>
                    <div class="company_info">
                        <div id="block-block-27" class="block block-block">
                            <div class="content">
                                <p class="ci_title">Услуги предоставляет:</p>
                                <p class="ci_text"><span>ООО «МКК Автокапитал»</span><span>ОГРН: 5177746069584</span><span>ИНН: 9710039070</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_bot">
        <div class="wrapper">
            <div class="mob_bg">
                <div class="mob_company_info">
                    <div class="company_info">
                        <div id="block-block-28" class="block block-block">
                            <div class="content">
                                <p class="ci_title">Услуги предоставляет:</p>
                                <p class="ci_text"><span>ООО «МКК Автокапитал»</span><span>ОГРН: 5177746069584</span><span>ИНН: 9710039070</span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="f_logo_mob">
                    <a>
                        <img class="dl" src="images/logo_new_foot.svg" alt="">
                        <img class="ml" src="images/logo_new_foot.svg" alt="">
                    </a>
                </div>
                <div id="block-block-29" class="block block-block">
                    <div class="content">
                        <p class="copyright">© &nbsp;«MZ24.RU», 2019</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
