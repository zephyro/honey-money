<div class="topmenu">
    <div class="wrapper">
        <ul class="flex align-items-center justify-content-between">
            <li><a href="/" class="">Главная</a></li>
            <li class="tm_toggle_link">
                <a href="#" class="active">О компании</a>
                <div class="tm_toggle">
                    <ul>
                        <li><a href="#">О нас</a></li>
                        <li><a href="#">Новости и акции</a></li>
                        <li><a href="#">Документы</a></li>
                    </ul>
                </div>
            </li>
            <li class="tm_toggle_link">
                <a href="#" class="">Как это работает</a>
                <div class="tm_toggle">
                    <ul>
                        <li><a href="#">Порядок действий</a></li>
                        <li><a href="#">Наши ставки</a></li>
                        <li><a href="#">Как перенести дату возврата</a></li>
                        <li><a href="#">Бонусная программа</a></li>
                    </ul>
                </div>
            </li>
            <li><a href="#" class="">Как получить</a></li>
            <li><a href="#" class="">Как погасить</a></li>
            <li><a href="#" class=""><span class="d_text">Вопросы и ответы</span><span class="mob_text">FAQ</span></a></li>
        </ul>
    </div>
</div>

<div class="mobile_menu">
    <div class="wrapper">
        <a class="mm_close"></a>
    </div>
    <div class="mobile_scroll">
        <div class="wrapper">
            <a class="mm_close"></a>
            <a href="//" class="mm_link1">Главная</a>
            <div class="mm_toggle_block">
                <a href="#/" class="mmt_main active">О компании<span></span></a>
                <div class="mm_toggle">
                    <a href="#">О нас</a>
                    <a href="#">Новости и акции</a>
                    <a href="#">Документы</a>
                </div>
            </div>
            <div class="mm_toggle_block">
                <a href="#" class="mmt_main ">Как это работает<span></span></a>
                <div class="mm_toggle">
                    <a href="#">Порядок действий</a>
                    <a href="#">Наши ставки</a>
                    <a href="#">Как перенести дату возврата</a>
                    <a href="#">Бонусная программа</a>
                </div>
            </div>
            <a href="#" class="mm_link1">Как получить</a>
            <a href="#" class="mm_link1">Как погасить</a>
            <a href="#" class="mm_link1">Вопросы и ответы</a>


            <div class="mm_personal_info">

            </div>
        </div>
    </div>
    <div class="mm_bottom">
        <div class="wrapper">
            <div class="mm_btns flex justify-content-center">
                <a href="/loan/" class="mm_btn1">Получить деньги</a>
                <a href="/auth/" class="mm_btn2">Личный кабинет</a>
            </div>
            <div class="mmp_wp">
                <div class="mm_phone">
                    <div id="block-block-2" class="block block-block">


                        <div class="content">
                            <p><a class="header_phone" href="tel:88002227434">8 (800) 222 74 34</a></p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="overlay"></div>