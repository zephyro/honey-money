<!DOCTYPE html>
<html>

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body class="mobile_fixed lk_page">

        <div class="page_wp page_gray">

            <!-- Header -->
            <?php include('inc/header_lk.inc.php') ?>
            <!-- -->

            <!-- Top menu -->
            <?php include('inc/topmenu.inc.php') ?>
            <!-- -->

            <div class="box">
                <div class="box__heading">
                    Подтверждение <br/>согласия с условиями <br/>доп. соглашения
                </div>
                <div class="box__info">
                    На номер <span class="color_red text_base">+7 (999) 777-11-00</span> было отправлено cmc с кодом подтверждения. Введите данный код, чтобы подписать доп. соглашение о продлении срока займа.
                </div>
                <div class="box__content box__content_white">
                    <div class="code">
                        <div class="code__label"><span>Код из СМС</span></div>
                        <input class="code__input" type="text" placeholder="123456" value=""/>
                    </div>
                    <a href="#modal6" class="code_repeat" data-fancybox="">
                        <i>
                            <img src="images/ico33.png" class="img_fluid" alt=""/>
                        </i>
                        <span>Выслать код повторно</span>
                    </a>
                </div>
                <div class="box__footer">
                    <button type="submit" class="btn btn_red btn_shadow">Подтвердить</button>
                </div>
            </div>


            <!-- Footer LC -->
            <?php include('inc/footer_lk.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->


        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
