

$(".range_extension").ionRangeSlider();


var partners = new Swiper('.partners__slider',{
    loop: true,
    slidesPerView: 4,
    spaceBetween: 10,
    nextButton: '.slider_next',
    prevButton: '.slider_prev',
    pagination: '.swiper-pagination',
    paginationClickable: true,
    breakpoints: {
        1199: {
            slidesPerView: 3,
        },
        767: {
            slidesPerView: 1
        }
    }
});


(function() {

    $('.select_day__label').on('click touchstart', function(e){
        e.preventDefault();
        $(this).closest('.select_day').toggleClass('open');
    });

    $('.select_day__dropdown li').on('click touchstart', function(e){
        e.preventDefault();
        $(this).closest('.select_day').removeClass('open');
        var day =  $(this).find('span').text();
        $(this).closest('.select_day').find('.select_day__label_value').text(day);
    });

}());

// Hide dropdown

$('body').click(function (event) {

    if ($(event.target).closest(".select_day").length === 0) {
        $(".select_day").removeClass('open');

    }
});

(function($){
    $(window).on("load",function(){
        $(".main_modal__scroll").mCustomScrollbar({
            theme:"dark-3"
        });
    });
})(jQuery);

