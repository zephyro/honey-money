﻿(function($){
	$(document).ready(function() {
		$btn_name_foot = "Личный кабинет";
		$btn_name_foot_s = "Личный кабинет";
		$btn_ref = "/cabinet/"; 
		$.post('/form/get_state.php', 
		{
			get_auth_state: true
			}, function(response) {
			if (isJsonString(response))
			{
				var data = JSON.parse(response);
				
				if (data.result == false) 
				{
					$btn_ref = "/auth/";
				}
				$(".faq_na").show();
			}
			else
			{
				
				
				$("header").html(response);
				$btn_name_foot = "Выход из личного кабинета";
				$btn_name_foot_s = "Выход из ЛК";
				$btn_ref = "/form/kabinet/logout.php";
				$(".faq_a").show();
				
				
				if($(window).width()<=767)
				{
					$.post('/form/get_info.php', 
					{
						get_info: true
						}, function(response) {
						
						$(".mm_personal_info").html(response);
						});
					
					
				}
			}
			
			$("header").show();
			$(".mm_btn2_foot").html($btn_name_foot);
			$(".mm_btn2").html($btn_name_foot_s);
			$(".mm_btn2").attr("href", $btn_ref);
		});
		
		
		///
		var mySwiper1 = new Swiper('.swiper1',{
			loop: true,
			slidesPerView: 2,
			spaceBetween: 30,
			nextButton: '.next1',
			prevButton: '.prev1',
			pagination: '.sp1',
			paginationClickable: true,
			breakpoints: {
				767: {
					slidesPerView: 1
				}
			}
		});
		var mySwiper2 = new Swiper('#swiper2',{
			loop: false,
			slidesPerView: 1,
			nextButton: '.next2',
			prevButton: '.prev2',
			pagination: '.sp2',
			paginationClickable: true
		});
		var mySwiper3 = new Swiper('#swiper3',{
			loop: false,
			slidesPerView: 1,
			nextButton: '.next3',
			prevButton: '.prev3',
			pagination: '.sp3',
			paginationClickable: true
		});
		var mySwiper4 = new Swiper('#swiper4',{
			loop: true,
			slidesPerView: 1,
			nextButton: '.next4',
			prevButton: '.prev4',
			pagination: '.sp4',
			paginationClickable: true
		});
		if ($(window).width()>480)
		{
			var banner_main = new Swiper('#banner_main',{
				loop: false,
				slidesPerView: 1,
			});
		}
		var banner_main2 = new Swiper('#banner_main2',{
			loop: true,
			slidesPerView: 1,
		});
		$('.input_wp p').bind("click", function() {
			if($(this).prev().hasClass('jq-selectbox')){
				console.log('oke');
				$(this).prev().trigger('open');
			}
			else {
				$(this).parent().find('input').focus();
			}
		});
		$(".input_wp input, .input_wp textarea").each(function () {
			if ($(this).val() != '') {
				$(this).parent().addClass('focused');
			}
			else {
				$(this).parent().removeClass('focused');
			}
		});
		$('.input_wp input, .input_wp textarea').bind("click change keyup", function() {
			if ($(this).val() != '') {
				$(this).parent().addClass('focused');
			}
			else {
				$(this).parent().removeClass('focused');
			}
		});													
		$(".qb_top").click(function () {
			if ($(this).parent().hasClass('active')) {
				$(this).parent().removeClass('active');
				$(this).next().fadeOut(0);
			}
			else {
				$(this).parent().addClass('active');
				$(this).next().fadeIn(200);
			}
		});
		$('body').on('click', '.qb_top2', function(e) {
			if ($(this).parent().hasClass('active')) {
				$(this).parent().removeClass('active');
				$(this).next().fadeOut(0);
			}
			else {
				$(this).parent().addClass('active');
				$(this).next().fadeIn(200);
			}
		});
		$('body').on('click', '.mm_link', function(e) {
			$('.mobile_menu').fadeIn(200);
			$('.overlay').fadeIn(200);
		});
		$(".mm_close").click(function () {
			$('.mobile_menu').fadeOut(200);
			$('.overlay').fadeOut(200);
		});
		$(".mmt_main").click(function () {
			if($(this).hasClass('active')){
				$(this).removeClass('active');
				$(this).next().fadeOut(0);
			}
			else {
				$(this).addClass('active');
				$(this).next().fadeIn(200);
			}
		});
		$(".tm_toggle_link>a").click(function () {
			if($(this).hasClass('active')){
				$(this).removeClass('active');
				$(this).next().fadeOut(0);
			}
			else {
				$(".tm_toggle_link>a").removeClass('active');
				$(".tm_toggle_link>a").next().fadeOut(0);
				$(this).addClass('active');
				$(this).next().fadeIn(200);
			}
		});
		$("body").click(function (e) {
			$(".cb_info span").removeClass('active');
			$('.cbi_label').css('visibility','hidden');
			$('.cbi_label').animate({opacity: 0}, 200);
		});
		$(".cb_info span").click(function (e) {
			e.stopPropagation();
			if($(this).hasClass('active')){
				$(this).removeClass('active');
				$(this).next().css('visibility','hidden');
				$(this).next().animate({opacity: 0}, 200);
			}
			else {
				$(this).addClass('active');
				$(this).next().css('visibility','visible');
				$(this).next().animate({opacity: 1}, 200);
			}
		});
		
		
		$('select').styler({
			selectSmartPositioning: false
		});
	});
	
	function isJsonString(str) {
		try {
			JSON.parse(str);
			} catch (e) {
			return false;
		}
		return true;
	}
})(jQuery)