<!DOCTYPE html>
<html>

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body class="mobile_fixed">

        <div class="page_wp page_bg1">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Top menu -->
            <?php include('inc/topmenu.inc.php') ?>
            <!-- -->

            <div class="breadcrumbs">
                <div class="wrapper">
                    <div class="breadcrumb">
                        <span class="inline odd first"><a href="/">Главная</a></span>
                        <span class="delimiter">&gt;</span>
                        <span class="inline even last">Контакты</span>
                    </div>
                </div>
            </div>

            <div class="contact">
                <div class="wrapper">
                    <div class="contact__row">
                        <div class="contact__left">
                            <div class="contact__heading">
                                <div class="contact__heading_second">Клиентам</div>
                                <h1>Контакты</h1>
                            </div>
                            <div class="contact__mobile">
                                <div class="contact__title">Служба поддержки клиентов</div>
                                <ul class="contact__list">
                                   <li>
                                       <a class="main_phone" href="tel:88002227434">8 (800) 222-74-34</a>
                                       <div>Звонок по России бесплатный</div>
                                       <a class="contact_icon" href="tel:88002227434"></a>
                                   </li>
                                    <li><strong>e-mail: </strong>  <a href="mailto:ask@mz24.ru">ask@mz24.ru</a></li>
                                    <li>г. Москва, ул. Антонова-
                                        Овсеенко, д.15. стр. 3, ком. 9</li>
                                    <li><strong>Сайт: </strong>  <a href="#">www.mz24.ru</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="contact__right">
                            <div class="contact_form">
                                <div class="contact_form__title">
                                    <h3>Написать сообщение</h3>
                                </div>
                                <form class="form">
                                    <div class="contact_form__content">
                                        <div class="form_group">
                                            <div class="input_wp">
                                                <input class="bonus_input" type="text">
                                                <p>E-mail</p>
                                            </div>
                                        </div>
                                        <div class="form_group">
                                            <div class="input_wp">
                                                <textarea class="rf_pole"></textarea>
                                                <p>Текст сообщения</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="contact_form__submit">
                                        <button type="submit" class="btn btn_xs_invert">Отправить</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="contacts_box">
                <div class="wrapper">
                    <ul class="contacts_box__main">
                        <li>
                            <div class="contacts_box__title"><span><span class="block_sm">Служба</span> контроля <span class="block_xs">качества</span></span></div>
                            <div class="contacts_box__content">
                                <i><img src="img/icon__contact_email.png"></i>
                                <span><strong>e-mail: </strong><a href="mailto:control@mz24.ru">control@mz24.ru</a></span>
                            </div>
                        </li>
                        <li>
                            <div class="contacts_box__title"><span>Пресс-Служба</span></div>
                            <div class="contacts_box__content">
                                <i><img src="img/icon__contact_email.png"></i>
                                <span><strong>e-mail: </strong><a href="mailto:press@mz24.ru">press@mz24.ru</a></span>
                            </div>
                        </li>
                        <li>
                            <div class="contacts_box__title"><span>По вопросам <br/>сотрудничества</span></div>
                            <div class="contacts_box__content">
                                <i><img src="img/icon__contact_email.png"></i>
                                <span><strong>e-mail: </strong><a href="mailto:partner@mz24.ru">partner@mz24.ru</a></span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>

            <!-- Footer Base -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->


        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
