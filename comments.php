<!DOCTYPE html>
<html>

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body class="mobile_fixed">

        <div class="page_wp page_bg1">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Top menu -->
            <?php include('inc/topmenu.inc.php') ?>
            <!-- -->

            <div class="breadcrumbs">
                <div class="wrapper">
                    <div class="breadcrumb">
                        <span class="inline odd first"><a href="/">Главная</a></span>
                        <span class="delimiter">&gt;</span>
                        <span class="inline odd first"><a href="#">Клиентам</a></span>
                        <span class="delimiter">&gt;</span>
                        <span class="inline even last">Отзывы наших клиентов</span>
                    </div>
                </div>
            </div>

            <div class="heading heading_border_sm">
                <div class="wrapper">
                    <div class="heading__inner">
                        <div class="heading__wrap">
                            <div class="heading__title">
                                <div class="heading__title_second">Клиентам</div>
                                <h1>Отзывы наших клиентов</h1>
                            </div>
                            <div class="heading__box">
                                <div class="heading__content">
                                    <h3 class="heading__subtitle">Поделитесь <span class="red_xs">мнением</span></h3>
                                    <div class="heading__image_mobile">
                                        <img src="img/comment__image.png" class="img_fluid" alt="">
                                    </div>
                                    <div class="heading__text">Нам интересно Ваше мнение, оставьте свой отзыв о работе нашего сервиса. Это поможет нам стать лучше.</div>

                                </div>
                                <ul class="heading__buttons">
                                    <li><a href="#" class="btn btn_red btn_shadow">Получить деньги</a></li>
                                    <li><a href="#" class="btn">Оставить отзыв</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="heading__image heading__image_tablet heading__image_comments">
                            <img src="img/comments__bg_tablet.png" class="img_fluid" alt="">
                        </div>
                        <div class="heading__image heading__image_desktop heading__image_comments">
                            <img src="img/comments__bg.png" class="img_fluid" alt="">
                        </div>
                    </div>
                </div>
            </div>

            <section class="comments">
                <div class="wrapper">

                    <div class="comments_heading">
                        <div class="comments_heading__second">О нашей компании</div>
                        <div class="comments_heading__primary"><span class="color_red">о нас</span> говорят</div>
                    </div>

                    <div class="comment_row">

                        <div class="comment">
                            <div class="comment__user">
                                <div class="comment__user_photo">
                                    <img src="img/comment__user_01.png" class="img_fluid" alt="">
                                </div>
                                <div class="comment__user_text">
                                    <div class="comment__user_name">Лариса Ивашенкова</div>
                                    <div class="comment__user_city">г. Пятигорск</div>
                                    <a href="#" class="comment__user_link"><i class="fa fa-vk"></i> <span>вконтакте</span></a>
                                </div>
                            </div>
                            <div class="comment__content">Интернет кардинально изменил нашу жизнь. Чтобы получить государственную услугу, сегодня достаточно зайти на портал госуслуг, узнать новости друзей можно в социальной сети «ВКонтакте»,  а получить</div>
                        </div>

                        <div class="comment">
                            <div class="comment__user">
                                <div class="comment__user_photo">
                                    <img src="img/comment__user_02.png" class="img_fluid" alt="">
                                </div>
                                <div class="comment__user_text">
                                    <div class="comment__user_name">Лариса Ивашенкова</div>
                                    <div class="comment__user_city">г. Пятигорск</div>
                                    <a href="#" class="comment__user_link"><i class="fa fa-vk"></i> <span>вконтакте</span></a>
                                </div>
                            </div>
                            <div class="comment__content">Интернет кардинально изменил нашу жизнь. Чтобы получить государственную услугу, сегодня достаточно зайти на портал госуслуг, узнать новости друзей можно в социальной сети «ВКонтакте»,  а получить</div>
                        </div>

                        <div class="comment">
                            <div class="comment__user">
                                <div class="comment__user_photo">
                                    <img src="img/comment__user_03.png" class="img_fluid" alt="">
                                </div>
                                <div class="comment__user_text">
                                    <div class="comment__user_name">Лариса Ивашенкова</div>
                                    <div class="comment__user_city">г. Пятигорск</div>
                                    <a href="#" class="comment__user_link"><i class="fa fa-vk"></i> <span>вконтакте</span></a>
                                </div>
                            </div>
                            <div class="comment__content">Интернет кардинально изменил нашу жизнь. Чтобы получить государственную услугу, сегодня достаточно зайти на портал госуслуг, узнать новости друзей можно в социальной сети «ВКонтакте»,  а получить</div>
                        </div>

                        <div class="comment">
                            <div class="comment__user">
                                <div class="comment__user_photo">
                                    <img src="img/comment__user_04.png" class="img_fluid" alt="">
                                </div>
                                <div class="comment__user_text">
                                    <div class="comment__user_name">Лариса Ивашенкова</div>
                                    <div class="comment__user_city">г. Пятигорск</div>
                                    <a href="#" class="comment__user_link"><i class="fa fa-vk"></i> <span>вконтакте</span></a>
                                </div>
                            </div>
                            <div class="comment__content">Интернет кардинально изменил нашу жизнь. Чтобы получить государственную услугу, сегодня достаточно зайти на портал госуслуг, узнать новости друзей можно в социальной сети «ВКонтакте»,  а получить</div>
                        </div>

                    </div>

                    <div class="text_center">
                        <button type="button" class="btn btn_lg btn_line">Показать еще</button>
                    </div>

                </div>
            </section>

            <!-- Footer Base -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->


        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
