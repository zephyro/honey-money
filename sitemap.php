<!DOCTYPE html>
<html>

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body class="mobile_fixed">

        <div class="page_wp page_bg1">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Top menu -->
            <?php include('inc/topmenu.inc.php') ?>
            <!-- -->

            <div class="breadcrumbs">
                <div class="wrapper">
                    <div class="breadcrumb">
                        <span class="inline odd first"><a href="/">Главная</a></span>
                        <span class="delimiter">&gt;</span>
                        <span class="inline even last">Карта сайта</span>
                    </div>
                </div>
            </div>

            <div class="sitemap">
                <div class="wrapper">
                    <div class="sitemap__heading">
                        <div class="contact__heading_second">Клиентам</div>
                        <h1>Карта сайта</h1>
                    </div>
                    <div class="sitemap__group">
                        <div class="sitemap__group_title">Раздел: <span class="color_red block_xs">О компании</span></div>
                        <div class="sitemap__group_content">
                            <div class="sitemap__group_subtitle">Подраздел: <span class="color_red">КаК получить</span></div>
                            <ul>
                                <li><a href="#">Какие документы нужны для получения займа</a></li>
                                <li><a href="#">Условия получения займа</a></li>
                                <li><a href="#">Займ для постоянных клиентов</a></li>
                                <li><a href="#">Задолженность по займу: как узнать сумму долга</a></li>
                                <li><a href="#">Оплатить займ : онлайн, без процентов</a></li>
                                <li><a href="#">Займ под залог</a></li>
                                <li><a href="#">Займы: условия, отзывы</a></li>
                                <li><a href="#">Онлайн заявка на займ</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="sitemap__group">
                        <div class="sitemap__group_title">Раздел: <span class="color_red block_xs">О компании</span></div>
                        <div class="sitemap__group_content">
                            <div class="sitemap__group_subtitle">Подраздел: <span class="color_red"> КаК ‘это работает</span></div>
                            <ul>
                                <li><a href="#">Какие документы нужны для получения займа</a></li>
                                <li><a href="#">Условия получения займа</a></li>
                                <li><a href="#">Займ для постоянных клиентов</a></li>
                                <li><a href="#">Задолженность по займу: как узнать сумму долга</a></li>
                                <li><a href="#">Оплатить займ : онлайн, без процентов</a></li>
                                <li><a href="#">Займ под залог</a></li>
                                <li><a href="#">Займы: условия, отзывы</a></li>
                                <li><a href="#">Онлайн заявка на займ</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>


            <div class="how_to">
                <div class="wrapper">
                    <div class="how_to__box">
                        <div class="how_to__content">
                            <div class="how_to__heading">Всего <span class="color_red">15 минут</span></div>
                            <div class="how_to__text">и деньги у Вас на карте</div>
                            <div class="how_to__image">
                                <img src="images/img22.png" alt="" class="img_fluid">
                            </div>
                        </div>
                        <div class="how_to__button">
                            <a href="#" class="btn btn_red btn_shadow">Получить деньги</a>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer Base -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->


        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
