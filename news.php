<!DOCTYPE html>
<html>

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body class="mobile_fixed">

        <div class="page_wp page_bg1">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Top menu -->
            <?php include('inc/topmenu.inc.php') ?>
            <!-- -->

            <div class="breadcrumbs">
                <div class="wrapper">
                    <div class="breadcrumb">
                        <span class="inline odd first"><a href="/">Главная</a></span>
                        <span class="delimiter">&gt;</span>
                        <span class="inline even last">Карьера</span>
                    </div>
                </div>
            </div>

            <div class="news_header">
                <div class="wrapper">
                    <div class="heading__title">
                        <div class="heading__title_second">Клиентам</div>
                        <h1>Новости <span class="color_red">и Акции</span></h1>
                    </div>
                </div>
            </div>

            <div class="news_intro">
                <div class="wrapper">
                    <div class="news_mobile">
                        <div class="news_intro__row">
                            <div class="news_intro__image">
                                <div class="news_intro__image_wrap">
                                    <img src="img/news_image.jpg" class="img_fluid" alt="">
                                    <div class="news_intro__label">Акции</div>
                                </div>
                            </div>
                            <div class="news_intro__content">
                                <div class="news_intro__date">21.02.2019</div>
                                <div class="news_intro__title">Клиенты сервисов <br/>онлайн-кредитования</div>
                                <div class="news_intro__text">
                                    Молодежи везде у нас дорога! Акция для заёмщиков до 35 лет! Скидка 35% от стандартной процентной ставки. С Мультизайм всегда все просто. Нужны деньги – получи онлайн-заём. Для всех заёмщиков до 35 лет в честь Дня Молодежи с 20 июня по 1 июля снижена процентная ставка на 35%. Танцуйте пока молодые вместе с Мультизайм.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="news_content">
                <div class="wrapper">
                    <div class="news_mobile">
                        <div class="news_content__wrap">
                            <h4>1. Организатор Акции</h4>
                            <p>1.1. ООО МФК «Мультизайм» (ИНН: 773001001, БИК: 044525593, юридический адрес: 121096, г. Москва, ул. Василисы Кожиной, д.1, офис Д13 (далее по тексту – «Организатор»). </p>
                            <h4>2. Сроки проведения Акции “ Отсентябрим 2019”</h4>
                            <p>2.1 Срок проведения Акции — с 14 августа по 27 августа 2019 г. включительно.</p>
                            <p>2.2. Объявление результатов акции состоится 28 августа в 12.00 на сайте <a href="#">www.mz24.ru</a>.</p>
                            <p>2.3. По усмотрению Организатора период проведения Акции и дата объявления результатов могут быть изменены, но на срок не более 5 (пяти) календарных дней.</p>
                            <h4>3. Для информирования участников Акции настоящие Правила размещены в сети Интернет по адресу:  <a href="#">www.mz24.ru</a>.</h4>
                            <h4>4. Определение победителей</h4>
                            <p>4.1. Победители Акции будут определены генератором случайных чисел.</p>
                            <p>4.2 Результаты Акции размещаются на сайте <a href="#">www.mz24.ru</a>.</p>
                            <h4>5. Призовой фонд Акции:</h4>
                            <p>5.1. 66 сертификатов в розничные магазины номиналом в 4000 рублей каждый.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="how_to">
                <div class="wrapper">
                    <div class="how_to__box">
                        <div class="how_to__content">
                            <div class="how_to__heading">Всего <span class="color_red">15 минут</span></div>
                            <div class="how_to__text">и деньги у Вас на карте</div>
                            <div class="how_to__image">
                                <img src="images/img22.png" alt="" class="img_fluid">
                            </div>
                        </div>
                        <div class="how_to__button">
                            <a href="#" class="btn btn_red btn_shadow">Получить деньги</a>
                        </div>
                    </div>
                </div>
            </div>


            <!-- Footer Base -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->


        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
