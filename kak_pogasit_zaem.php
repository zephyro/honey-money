 <!DOCTYPE html>
<html>

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body class="mobile_fixed">

        <div class="page_wp page_bg1">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Top menu -->
            <?php include('inc/topmenu.inc.php') ?>
            <!-- -->

            <div class="breadcrumbs">
                <div class="wrapper">
                    <div class="breadcrumb">
                        <span class="inline odd first"><a href="/">Клиентам</a></span>
                        <span class="delimiter">&gt;</span>
                        <span class="inline even last">Как погасить</span>
                    </div>
                </div>
            </div>

            <div class="repay">
                <div class="wrapper">
                    <div class="repay__heading">
                        <div class="repay__heading_top">Клиентам</div>
                        <h1 class="repay__heading_title"><span class="color_red">способы</span> оплаты</h1>
                        <div class="repay__heading_text">Вы можете произвести оплату по займу банковской картой прямо на нашем сайте</div>
                    </div>
                    <div class="repay__mobile">
                        <div class="repay__mobile_title">Подзаголовок как <span class="color_red">погасить</span></div>
                        <div class="repay__content">
                            <div class="repay__content_left">
                                <div class="repay__card">
                                    <img src="images/img4-1.png" class="img_fluid" alt="">
                                </div>
                                <div class="repay__pay">
                                    <img src="img/repay_system.png" class="img_fluid" alt="">
                                </div>
                            </div>
                            <div class="repay__content_center">
                                <div class="repay__text">
                                    <div class="repay__text_title">Банковской картой</div>
                                    <p>В режиме оnline на нашем сайте</p>
                                </div>
                            </div>
                            <div class="repay__content_right">
                                <ul class="repay__info">
                                    <li>Без комиссии</li>
                                    <li>Моментальное зачисление</li>
                                </ul>
                                <div class="repay__pay_mobile">
                                    <img src="img/repay_system.png" class="img_fluid" alt="">
                                </div>
                                <div class="repay__button">
                                    <a href="#" class="btn btn_red btn_shadow">Оплатить</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="manual">
                <div class="wrapper">
                    <div class="mob_p1">

                        <h2 class="manual__heading"><span class="color_red">Пошаговая инструкция</span><br/>по оплате займа</h2>

                        <div class="instruction_wp">

                            <div class="instruction_block flex justify-content-between">
                                <div class="instruction_img">

                                </div>
                                <div class="instruction_info">
                                    <div id="block-block-86" class="block block-block">
                                        <div class="content">
                                            <p class="ii_title"><span>Первый</span> подзаголовок:</p>
                                            <p class="ii_text">На главной странице <span>МультиЗайм</span> выберите срок и сумму займа и нажмите кнопку ”Получить деньги” для перехода к следующему шагу.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="instruction_block ib_v1 flex justify-content-between">
                                <div class="instruction_img">

                                </div>
                                <div class="instruction_info">
                                    <div id="block-block-87" class="block block-block">
                                        <div class="content">
                                            <p class="ii_title"><span>Второй</span> подзаголовок:</p>
                                            <p class="ii_text">На главной странице <span>МультиЗайм</span> выберите срок и сумму займа и нажмите кнопку ”Получить деньги” для перехода к следующему шагу.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="instruction_block flex justify-content-between">
                                <div class="instruction_img">

                                </div>
                                <div class="instruction_info">
                                    <div id="block-block-86" class="block block-block">
                                        <div class="content">
                                            <p class="ii_title"><span>Третий</span> подзаголовок:</p>
                                            <p class="ii_text">На главной странице <span>МультиЗайм</span> выберите срок и сумму займа и нажмите кнопку ”Получить деньги” для перехода к следующему шагу.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="instruction_block ib_v1 flex justify-content-between">
                                <div class="instruction_img">

                                </div>
                                <div class="instruction_info">
                                    <div id="block-block-87" class="block block-block">
                                        <div class="content">
                                            <p class="ii_title"><span>Четвертый</span> подзаголовок:</p>
                                            <p class="ii_text">На главной странице <span>МультиЗайм</span> выберите срок и сумму займа и нажмите кнопку ”Получить деньги” для перехода к следующему шагу.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="instruction_block flex justify-content-between">
                                <div class="instruction_img">

                                </div>
                                <div class="instruction_info">
                                    <div id="block-block-86" class="block block-block">
                                        <div class="content">
                                            <p class="ii_title"><span>Пятый</span> подзаголовок:</p>
                                            <p class="ii_text">На главной странице <span>МультиЗайм</span> выберите срок и сумму займа и нажмите кнопку ”Получить деньги” для перехода к следующему шагу.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="instruction_block ib_v1 flex justify-content-between">
                                <div class="instruction_img">

                                </div>
                                <div class="instruction_info">
                                    <div id="block-block-87" class="block block-block">
                                        <div class="content">
                                            <p class="ii_title"><span>Шестой</span> подзаголовок:</p>
                                            <p class="ii_text">На главной странице <span>МультиЗайм</span> выберите срок и сумму займа и нажмите кнопку ”Получить деньги” для перехода к следующему шагу.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="repay_box">
                <div class="wrapper">
                    <div class="repay_box__mobile">
                        <div class="repay_box__mobile_title"><span class="color_red block_xs">Досрочное</span> погашение займа</div>
                        <div class="repay_box__wrap">
                            <div class="repay_box__image">
                                <div class="repay_box__image_wrap">
                                    <img src="img/repay_box__image.png" class="img_fluid" alt="">
                                </div>
                            </div>
                            <div class="repay_box__content">
                                <h2 class="repay_box__title">Досрочное <span class="block_sm_only">погашение займа</span></h2>
                                <div class="repay_box__primary">Вы можете <strong>досрочно</strong> погасить заем в любой день без переплаты и комиссий. Проценты по займу начисляются строго за срок фактического пользования займом.</div>
                                <div class="repay_box__second">Чтобы произвести досрочное погашение зайдите в раздел «Открытый заем» в личный кабинет на нашем сайте. Далее нажмите кнопку «Погасить заем», введите сумму для полного погашения и нажмите кнопку «Оплатить». После зачисления денег Вам придёт смс с подтверждением закрытия займа.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="how_to">
                <div class="wrapper">
                    <div class="how_to__box">
                        <div class="how_to__content">
                            <div class="how_to__heading">Всего <span class="color_red">15 минут</span></div>
                            <div class="how_to__text">и деньги у Вас на карте</div>
                            <div class="how_to__image">
                                <img src="images/img22.png" alt="" class="img_fluid">
                            </div>
                        </div>
                        <div class="how_to__button">
                            <a href="#" class="btn btn_red btn_shadow">Получить деньги</a>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer Base -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->


        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
