<!DOCTYPE html>
<html>

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body class="mobile_fixed">

        <div class="page_wp page_bg1">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Top menu -->
            <?php include('inc/topmenu.inc.php') ?>
            <!-- -->

            <div class="breadcrumbs">
                <div class="wrapper">
                    <div class="breadcrumb">
                        <span class="inline odd first"><a href="/">Клиентам</a></span>
                        <span class="delimiter">&gt;</span>
                        <span class="inline even last">Партнерская программа</span>
                    </div>
                </div>
            </div>

            <div class="heading heading_partners">
                <div class="wrapper">
                    <div class="heading__inner">
                        <div class="heading__wrap">
                            <div class="heading__title">
                                <div class="heading__title_second">Клиентам</div>
                                <h1>Партнерская программа</h1>
                            </div>
                            <div class="heading__box">
                                <div class="heading__content">
                                    <h3 class="heading__subtitle">Зарабатывайте вместе <span class="block_xs red_xs">с мультизайм</span></h3>
                                    <div class="heading__image_mobile">
                                        <img src="img/partners_image.jpg" class="img_fluid" alt="">
                                    </div>
                                    <div class="heading__text">Сотрудничая с Мультизайм вы получаете: высокие тарифы, отличную конверсию, статистику в режиме реального времени, быстрые выплаты. Мы открыты для новых партнеров. Отправьте заявку и начнем зарабатывать вместе!</div>

                                </div>
                            </div>
                        </div>
                        <div class="heading__image heading__image_tablet">
                            <img src="img/partners__bg_tablet.png" class="img_fluid" alt="">
                        </div>
                        <div class="heading__image heading__image_desktop">
                            <img src="img/partners__bg_desktop.png" class="img_fluid" alt="">
                        </div>
                    </div>
                </div>
            </div>

            <div class="partners_step">
                <div class="wrapper">
                    <div class="partners_step__container">
                        <div class="partners_step__row">
                            <div class="partners_step__item partners_step__item_one">
                                <div class="partners_step__item_number"><span>1</span></div>
                                <div class="partners_step__item_text">Подайте заявку</div>
                            </div>
                            <div class="partners_step__item partners_step__item_two">
                                <div class="partners_step__item_number"><span>2</span></div>
                                <div class="partners_step__item_text">Приведите клиентов</div>
                            </div>
                            <div class="partners_step__item partners_step__item_three">
                                <div class="partners_step__item_number"><span>3</span></div>
                                <div class="partners_step__item_text">Получайте доход</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <section class="main">
                <div class="wrapper">
                    <div class="main__mobile">
                        <div class="main__heading">
                            <h2>Что мы <span class="main__heading_red block_xs">предлагаем</span></h2>
                        </div>
                        <div class="partner__row">
                            <div class="partner">
                                <div class="partner__image">
                                    <div class="partner__image_wrap">
                                        <img src="img/partners__image_01.png" class="img_fluid" alt="">
                                    </div>
                                </div>
                                <a href="#partner01" class="partner__text" data-fancybox>
                                    <span>Веб-мастерам</span>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>

                            <div class="partner">
                                <div class="partner__image">
                                    <div class="partner__image_wrap">
                                        <img src="img/partners__image_02.png" class="img_fluid" alt="">
                                    </div>
                                </div>
                                <a href="#partner02" class="partner__text" data-fancybox>
                                    <span>Владельцам <br/>сайтов</span>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>

                            <div class="partner">
                                <div class="partner__image">
                                    <div class="partner__image_wrap">
                                        <img src="img/partners__image_03.png" class="img_fluid" alt="">
                                    </div>
                                </div>
                                <a href="#partner03" class="partner__text" data-fancybox>
                                    <span>Партнерским <br/>сетям</span>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </div>
                        <div class="partner__button">
                            <a href="#" class="btn btn_red btn_shadow">Отправить заявку</a>
                        </div>
                    </div>
                </div>
            </section>

            <div class="partners">
                <div class="wrapper">
                    <div class="partners__mobile">
                        <div class="partners__heading">
                            <h2>Наши <span>партнеры</span></h2>
                        </div>
                        <div class="partners__wrap">
                            <div class="partners__slider swiper-container">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="partners__logo">
                                            <img src="img/partners/partner__logo_01.png" class="img_fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="partners__logo">
                                            <img src="img/partners/partner__logo_02.png" class="img_fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="partners__logo">
                                            <img src="img/partners/partner__logo_03.png" class="img_fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="partners__logo">
                                            <img src="img/partners/partner__logo_04.png" class="img_fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="partners__logo">
                                            <img src="img/partners/partner__logo_01.png" class="img_fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="partners__logo">
                                            <img src="img/partners/partner__logo_02.png" class="img_fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="partners__logo">
                                            <img src="img/partners/partner__logo_03.png" class="img_fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="partners__logo">
                                            <img src="img/partners/partner__logo_04.png" class="img_fluid" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a class="slider_prev"></a>
                            <a class="slider_next"></a>
                        </div>
                        <div class="swiper-pagination"></div>
                    </div>
                </div>
            </div>

            <div class="form_block form_block__contact">
                <div class="wrapper">
                    <div class="form_block__mobile">
                        <form class="form">
                            <div class="form_block__heading">
                                <div class="hide_xs">
                                    <h2>Свяжитесь  <span class="color_red">с нами</span></h2>
                                </div>
                                <div class="hide_sm">
                                    <h2>Отправьте нам сообщение</h2>
                                </div>
                            </div>
                            <div class="form_block__content">
                                <div class="form__row">
                                    <div class="form__col">
                                        <div class="form_group">
                                            <div class="input_wp">
                                                <input type="text" placeholder="" class="rf_input"/>
                                                <p>ФИО</p>
                                            </div>
                                        </div>
                                        <div class="form__line">
                                            <div class="form__line_col">
                                                <div class="form_group">
                                                    <div class="input_wp">
                                                        <input type="text" placeholder="" class="rf_input"/>
                                                        <p>Телефон</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form__line_col">
                                                <div class="form_group">
                                                    <div class="input_wp">
                                                        <input type="text" placeholder="" class="rf_input"/>
                                                        <p>E-mail</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form__col">
                                        <div class="form_group">
                                            <div class="input_wp">
                                                <textarea class="rf_pole" placeholder=""></textarea>
                                                <p>Текст сообщения</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form_block__submit">
                                <button type="submit" class="btn btn_xs_invert">Отправить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Footer Base -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <div class="hide">
            <div class="modal modal_w1 main_modal" id="partner01">
                <div class="main_modal__scroll">
                    <div class="main_modal__content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget feugiat purus. Duis vitae ipsum sed lorem porta elementum ullamcorper hendrerit risus. Morbi nec pulvinar enim, eu efficitur lacus. Suspendisse eget neque at odio placerat mattis. In id vestibulum est. Fusce condimentum gravida ex et rutrum. Vestibulum nec tempor orci, eget sodales ante.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget feugiat purus. Duis vitae ipsum sed lorem porta elementum ullamcorper hendrerit risus. Morbi nec pulvinar enim, eu efficitur lacus. Suspendisse eget neque at odio placerat mattis. In id vestibulum est. Fusce condimentum gravida ex et rutrum. Vestibulum nec tempor orci, eget sodales ante.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget feugiat purus. Duis vitae ipsum sed lorem porta elementum ullamcorper hendrerit risus. Morbi nec pulvinar enim, eu efficitur lacus. Suspendisse eget neque at odio placerat mattis. In id vestibulum est. Fusce condimentum gravida ex et rutrum. Vestibulum nec tempor orci, eget sodales ante.</p>
                    </div>
              </div>
            </div>
        </div>


        <div class="hide">
            <div class="modal modal_w1 main_modal" id="partner02">
                <div class="main_modal__scroll">
                    <div class="main_modal__content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget feugiat purus. Duis vitae ipsum sed lorem porta elementum ullamcorper hendrerit risus. Morbi nec pulvinar enim, eu efficitur lacus. Suspendisse eget neque at odio placerat mattis. In id vestibulum est. Fusce condimentum gravida ex et rutrum. Vestibulum nec tempor orci, eget sodales ante.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget feugiat purus. Duis vitae ipsum sed lorem porta elementum ullamcorper hendrerit risus. Morbi nec pulvinar enim, eu efficitur lacus. Suspendisse eget neque at odio placerat mattis. In id vestibulum est. Fusce condimentum gravida ex et rutrum. Vestibulum nec tempor orci, eget sodales ante.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget feugiat purus. Duis vitae ipsum sed lorem porta elementum ullamcorper hendrerit risus. Morbi nec pulvinar enim, eu efficitur lacus. Suspendisse eget neque at odio placerat mattis. In id vestibulum est. Fusce condimentum gravida ex et rutrum. Vestibulum nec tempor orci, eget sodales ante.</p>
                    </div>
                </div>
            </div>
        </div>


        <div class="hide">
            <div class="modal modal_w1 main_modal" id="partner03">
                <div class="main_modal__scroll">
                    <div class="main_modal__content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget feugiat purus. Duis vitae ipsum sed lorem porta elementum ullamcorper hendrerit risus. Morbi nec pulvinar enim, eu efficitur lacus. Suspendisse eget neque at odio placerat mattis. In id vestibulum est. Fusce condimentum gravida ex et rutrum. Vestibulum nec tempor orci, eget sodales ante.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget feugiat purus. Duis vitae ipsum sed lorem porta elementum ullamcorper hendrerit risus. Morbi nec pulvinar enim, eu efficitur lacus. Suspendisse eget neque at odio placerat mattis. In id vestibulum est. Fusce condimentum gravida ex et rutrum. Vestibulum nec tempor orci, eget sodales ante.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget feugiat purus. Duis vitae ipsum sed lorem porta elementum ullamcorper hendrerit risus. Morbi nec pulvinar enim, eu efficitur lacus. Suspendisse eget neque at odio placerat mattis. In id vestibulum est. Fusce condimentum gravida ex et rutrum. Vestibulum nec tempor orci, eget sodales ante.</p>
                    </div>
                </div>
            </div>
        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
