<!DOCTYPE html>
<html>

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body class="mobile_fixed lk_page">

        <div class="page_wp page_bg1">

            <!-- Header -->
            <?php include('inc/header_lk.inc.php') ?>
            <!-- -->

            <!-- Top menu -->
            <?php include('inc/topmenu.inc.php') ?>
            <!-- -->


            <div class="consent_wp lk_content my_lk">
                <div class="wrapper">
                    <div class="lk_wp2 flex align-items-start justify-content-between">

                        <div class="lk_left">

                            <form class="form">

                                <div class="loan_title lt_mob_f1"><div class="text_uppercase">Отсрочка выплаты</div></div>

                                <div class="account">

                                    <div class="account__heading">
                                        <div class="account__heading_title">Выберите срок продления займа</div>
                                        <div class="account__heading_text">Отсрочка выплаты позволяет перенести дату оплаты на удобный Вам срок и избежать просрочки оплаты по займу. Вы экономите деньги на пенях и неустойках и не портите свою кредитную историю.</div>
                                    </div>

                                    <div class="account__data">

                                        <div class="account__data_term">
                                            <div class="value">
                                                <div class="value__label">
                                                    <span>Срок продления</span>
                                                </div>

                                                <div class="value__day">
                                                    <div class="select_day">
                                                        <div class="select_day__label">
                                                            <div class="select_day__label_value">5</div>
                                                            <div class="select_day__label_text">дней</div>
                                                            <div class="select_day__label_arrow">
                                                                <i class="fa fa-angle-down"></i>
                                                            </div>
                                                        </div>
                                                        <div class="select_day__dropdown">
                                                            <ul>
                                                                <li><span>7</span> дней</li>
                                                                <li><span>14</span> дней</li>
                                                                <li><span>21</span> дней</li>
                                                                <li><span>28</span> дней</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="account__data_amount">
                                            <div class="range_extension" id="range_extension"
                                                 data-type="single"
                                                 data-min="1"
                                                 data-max="28"
                                                 data-from="7"
                                                 data-to="28"
                                                 data-grid="false">
                                            </div>
                                            <p class="range_label flex justify-content-between">
                                                <span>7 дней</span>
                                                <span>28 дней</span>
                                            </p>
                                        </div>

                                        <div class="account__data_cost">
                                            <div class="value">
                                                <div class="value__label">
                                                    <span>Стоимость продления</span>
                                                </div>
                                                <div class="value__elem">
                                                    <input type="text" class="value__elem_input" value="700,00" name="range_extension_cost" id="range_extension_cost">
                                                    <span class="value__elem_label">₽</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="account__content">
                                        <div class="delay">
                                            <div class="delay__date">
                                                <div class="delay__date_label">
                                                    <div class="delay__wrap">Новая дата<br/>возврата</div>
                                                </div>
                                                <div class="delay__date_value">
                                                    <div class="delay__wrap">12 июля 2019<br/>вторник</div>
                                                </div>
                                            </div>
                                            <div class="delay__sum">
                                                <div class="delay__sum_title">
                                                    <div class="delay__wrap">Сумма к погашению</div>
                                                </div>
                                                <div class="delay__sum_item delay__sum_active">
                                                    <div class="delay__sum_label">
                                                        <div class="delay__wrap">С продлением</div>
                                                    </div>
                                                    <div class="delay__sum_value">
                                                        <div class="delay__wrap">11 740,00 <span>₽</span></div>
                                                    </div>
                                                </div>
                                                <div class="delay__sum_item">
                                                    <div class="delay__sum_label">
                                                        <div class="delay__wrap">С просрочкой</div>
                                                    </div>
                                                    <div class="delay__sum_value">
                                                        <div class="delay__wrap">13 740,00 <span>₽</span></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="account__submit">
                                    <button type="submit" class="btn btn_red btn_shadow">Оформить продление</button>
                                </div>


                            </form>

                        </div>

                        <div class="lk_right visible_lg">
                            <div class="mob_bonus_f mob_bonus_one bb_p_dn">
                                <div class="bonus_block">
                                    <p class="bonus_title">Мои Бонусы</p>
                                    <div class="bonus_info flex align-items-center justify-content-center">
                                        <img src="images/ico41.png" alt=""/><p>500</p>
                                    </div>
                                    <div class="lk_links flex justify-content-between">
                                        <a>Потратить</a>
                                        <a>Заработать</a>
                                    </div>
                                </div>
                            </div>
                            <div class="bonus_block bonus_forma">
                                <p class="bonus_title">Написать сообщение</p>
                                <form class="bonus_form">
                                    <div class="bf_mob_wp">
                                        <div class="bf_mob">
                                            <div class="input_wp">
                                                <select data-placeholder="Пол">
                                                    <option></option>
                                                    <option>Тема сообщения</option>
                                                    <option>Тема сообщения</option>
                                                </select>
                                                <p>Тема сообщения</p>
                                            </div>
                                        </div>
                                        <div class="input_wp bonus_pole_wp">
                                            <textarea class="bonus_pole"></textarea>
                                            <p>Текст сообщения</p>
                                        </div>
                                    </div>
                                    <div class="bf_mob1">
                                        <div class="add_file af_mob_m1 flex align-items-center justify-content-center">
                                            <a><img src="images/ico43.png" alt=""/><span>Прикрепить файл</span></a>
                                        </div>
                                        <div class="lkf_bot lkf_mob_gray">
                                            <a data-fancybox href="#modal5" class="bf_btn">Отправить</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer LC -->
            <?php include('inc/footer_lk.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->


        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
