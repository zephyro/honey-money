<!DOCTYPE html>
<html>

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body class="mobile_fixed">

        <div class="page_wp page_bg1">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Top menu -->
            <?php include('inc/topmenu.inc.php') ?>
            <!-- -->

            <div class="breadcrumbs">
                <div class="wrapper">
                    <div class="breadcrumb">
                        <span class="inline odd first"><a href="/">Клиентам</a></span>
                        <span class="delimiter">&gt;</span>
                        <span class="inline even last">Наши ставки</span>
                    </div>
                </div>
            </div>

            <div class="rate_page_header">
                <div class="wrapper">
                    <div class="heading__title">
                        <div class="heading__title_second">Клиентам</div>
                        <h1>Наши ставки</h1>
                    </div>
                </div>
            </div>

            <div class="rate_block">
                <div class="wrapper">
                    <div class="rate">
                        <div class="rate__row">
                            <div class="rate__intro">
                                <div class="rate__heading">
                                    <strong>Программа <span class="color_red">Старт</span></strong>
                                    <i><img src="img/rate__title_icon_01.png" alt="" class="img_fluid"></i>
                                </div>
                                <div class="rate__intro_text">
                                    <h4>Для новых заемщиков</h4>
                                    <div>Ставка по займу 0%, заем предоставляется абсолютно бесплатно.</div>
                                </div>
                            </div>
                            <div class="rate__detail">
                                <div class="rate__detail_left">
                                    <div class="rate__detail_icon">
                                        <div class="rate__detail_icon__wrap">
                                            <i>
                                                <img src="img/rate__icon_lg_01.png" class="img_fluid" alt="">
                                            </i>
                                        </div>
                                    </div>
                                    <div class="rate__detail_mobile">
                                        <h4>Для новых заемщиков</h4>
                                        <div>Ставка по займу 0%, заем предоставляется абсолютно бесплатно.</div>
                                    </div>
                                </div>
                                <div class="rate__detail_right">
                                    <ul class="rate__detail_info">
                                        <li>
                                            <i><img src="img/rate__icon__01.png" class="img_fluid" alt=""></i>
                                            <span>Ставка 0% (бесплатно)</span>
                                        </li>
                                        <li>
                                            <i><img src="img/rate__icon__02.png" class="img_fluid" alt=""></i>
                                            <span>Попробуйте наш сервис</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <ul class="rate__info">
                            <li>
                                <div class="rate__info_title">
                                    <div class="rate__info_wrap">Сумма займа</div>
                                </div>
                                <div class="rate__info_value">
                                    <div class="rate__info_wrap">
                                        от 1 000 Р<br/>
                                        до 15 000 Р
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="rate__info_title">
                                    <div class="rate__info_wrap">Срок</div>
                                </div>
                                <div class="rate__info_value">
                                    <div class="rate__info_wrap">от 5 до 15 дней</div>
                                </div>
                            </li>
                            <li>
                                <div class="rate__info_title">
                                    <div class="rate__info_wrap">Ставка</div>
                                </div>
                                <div class="rate__info_value">
                                    <div class="rate__info_wrap">0% в день</div>
                                </div>
                            </li>
                            <li>
                                <div class="rate__info_title">
                                    <div class="rate__info_wrap">Оплата</div>
                                </div>
                                <div class="rate__info_value">
                                    <div class="rate__info_wrap">Одним платежом в конце срока займа</div>
                                </div>
                            </li>
                        </ul>
                        <div class="rate__button">
                            <a href="#" class="btn btn_red btn_shadow">Получить деньги</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="rate_block rate_block_gray">
                <div class="wrapper">
                    <div class="rate">
                        <div class="rate__row">
                            <div class="rate__intro">
                                <div class="rate__heading">
                                    <strong>Программа <span class="color_red">Хит</span></strong>
                                    <i><img src="img/rate__title_icon_02.png" alt="" class="img_fluid"></i>
                                </div>
                                <div class="rate__intro_text">
                                    <h4>Для повторных заемщиков</h4>
                                    <div>Наиболее популярная программа для повторных заемщиков. Позволяет взять большую сумму на более длительный срок</div>
                                </div>
                            </div>
                            <div class="rate__detail">
                                <div class="rate__detail_left">
                                    <div class="rate__detail_icon">
                                        <div class="rate__detail_icon__wrap">
                                            <i>
                                                <img src="img/rate__icon_lg_02.png" class="img_fluid" alt="">
                                            </i>
                                        </div>
                                    </div>
                                    <div class="rate__detail_mobile">
                                        <h4>Для повторных заемщиков</h4>
                                        <div>Наиболее популярная программа для повторных заемщиков. Позволяет взять большую сумму на более длительный срок</div>
                                    </div>
                                </div>
                                <div class="rate__detail_right">
                                    <ul class="rate__detail_info">
                                        <li>
                                            <i><img src="img/rate__icon__03.png" class="img_fluid" alt=""></i>
                                            <span>Удобный <span class="block_sm">срок</span></span>
                                        </li>
                                        <li>
                                            <i><img src="img/rate__icon__04.png" class="img_fluid" alt=""></i>
                                            <span>Больше <span class="block_sm">сумма</span></span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <ul class="rate__info">
                            <li>
                                <div class="rate__info_title">
                                    <div class="rate__info_wrap">Сумма займа</div>
                                </div>
                                <div class="rate__info_value">
                                    <div class="rate__info_wrap">
                                        от 1 000 Р<br/>
                                        до 30 000 Р
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="rate__info_title">
                                    <div class="rate__info_wrap">Срок</div>
                                </div>
                                <div class="rate__info_value">
                                    <div class="rate__info_wrap">от 5 до 15 дней</div>
                                </div>
                            </li>
                            <li>
                                <div class="rate__info_title">
                                    <div class="rate__info_wrap">Ставка</div>
                                </div>
                                <div class="rate__info_value">
                                    <div class="rate__info_wrap">0,9% в день</div>
                                </div>
                            </li>
                            <li>
                                <div class="rate__info_title">
                                    <div class="rate__info_wrap">Оплата</div>
                                </div>
                                <div class="rate__info_value">
                                    <div class="rate__info_wrap">Одним платежом в конце срока займа</div>
                                </div>
                            </li>
                        </ul>
                        <div class="rate__button">
                            <a href="#" class="btn btn_red btn_shadow">Получить деньги</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="rate_block">
                <div class="wrapper">
                    <div class="rate">
                        <div class="rate__row">
                            <div class="rate__intro">
                                <div class="rate__heading">
                                    <strong>Программа <span class="color_red">макс</span></strong>
                                    <i><img src="img/rate__title_icon_03.png" alt="" class="img_fluid"></i>
                                </div>
                                <div class="rate__intro_text">
                                    <h4>Для постоянных заемщиков</h4>
                                    <div>Вы получаете максимальную сумму займа по минимальной ставке на длительный срок. Погашение займа равными платежами каждые две недели.</div>
                                </div>
                            </div>
                            <div class="rate__detail">
                                <div class="rate__detail_left">
                                    <div class="rate__detail_icon">
                                        <div class="rate__detail_icon__wrap">
                                            <i>
                                                <img src="img/rate__icon_lg_03.png" class="img_fluid" alt="">
                                            </i>
                                        </div>
                                    </div>
                                    <div class="rate__detail_mobile">
                                        <h4>Для новых заемщиков</h4>
                                        <div>Ставка по займу 0%, заем предоставляется абсолютно бесплатно.</div>
                                    </div>
                                </div>
                                <div class="rate__detail_right">
                                    <ul class="rate__detail_info">
                                        <li>
                                            <i><img src="img/rate__icon__05.png" class="img_fluid" alt=""></i>
                                            <span>Длительный срок</span>
                                        </li>
                                        <li>
                                            <i><img src="img/rate__icon__06.png" class="img_fluid" alt=""></i>
                                            <span>Максимальная сумма</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <ul class="rate__info">
                            <li>
                                <div class="rate__info_title">
                                    <div class="rate__info_wrap">Сумма займа</div>
                                </div>
                                <div class="rate__info_value">
                                    <div class="rate__info_wrap">
                                        от 30 000 Р<br/>
                                        до 60 000 Р
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="rate__info_title">
                                    <div class="rate__info_wrap">Срок</div>
                                </div>
                                <div class="rate__info_value">
                                    <div class="rate__info_wrap">от 10 до 20 недель</div>
                                </div>
                            </li>
                            <li>
                                <div class="rate__info_title">
                                    <div class="rate__info_wrap">Ставка</div>
                                </div>
                                <div class="rate__info_value">
                                    <div class="rate__info_wrap">0,4% в день</div>
                                </div>
                            </li>
                            <li>
                                <div class="rate__info_title">
                                    <div class="rate__info_wrap">Оплата</div>
                                </div>
                                <div class="rate__info_value">
                                    <div class="rate__info_wrap">Равными платежами раз в две недели</div>
                                </div>
                            </li>
                        </ul>
                        <div class="rate__button">
                            <a href="#" class="btn btn_red btn_shadow">Получить деньги</a>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer Base -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->


        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
